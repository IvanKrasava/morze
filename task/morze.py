def code_morze(value):
   # Write functions code_morse for Morse coding. Notes: The spacebars shoulf be ignored for coding. 
   # The Morse letters in coded message should be separated by spacebars

    Supplementary = {'A': '.-', 'B': '-...', 'C': '-.-.',
    'D': '-..', 'E': '.', 'F': '..-.', 'G': '--.', 
    'H': '....', 'I': '..', 'J': '.---', 'K': '-.-', 
    'L': '.-..', 'M': '--', 'N': '-.', 'O': '---', 
    'P': '.--.', 'Q': '--.-', 'R': '.-.', 'S': '...', 
    'T': '-', 'U': '..-', 'V': '...-', 'W': '.--', 'X': '-..-', 'Y': '-.--', 'Z': '--..',
    '1': '.----', '2': '..---', '3': '...--',
    '4': '....-', '5': '.....', '6': '-....',
    '7': '--...', '8': '---..', '9': '----.', '0': '-----', ', ': '--..--', 
    '.': '.-.-.-', '?': '..--..', '/': '-..-.', '-': '-....-', 
    '(': '-.--.', ')': '-.--.-'} 
    # Example of input: "Data Science - 2023" Example of output: 
    # "−·· ·− − ·− ··· −·−· ·· · −· −·−· · −····− ··−−− −−−−− ··−−− ···−−",
    result = ""
    for i in value:
        if i != " " and i != ',':
            j = str.upper(i)
            result = result + Supplementary[j] + " "
        elif i == ',':
            result = result + Supplementary[", "] + " "
        
    result = result[:-1]
    result = "'" + result + "'"
    return result
